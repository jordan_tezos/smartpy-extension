# Smartpy Extension

Smartpy Extension provides new features for the [Smartpy library](https://gitlab.com/SmartPy/smartpy).

It provides decorators and mixins to facilitate and standardize Tezos Smart Contracts development with Smartpy.

Author: Jordan Tez

Email: jordan.tezos@gmail.com

Twitter: https://mobile.twitter.com/jordan_tezos

This extension is not officially distributed by the Smartpy team.
It will probably be replaced later by something official, more complete and consistent.
It could also be integrated into smartpy in a near future.

<!-- MarkdownTOC autolink="true" style="ordered" -->

1. [Import](#import)
1. [Rights management](#rights-management)
    1. [@admin_only](#admin_only)
    1. [@allow_only\(\*args\)](#allow_onlyargs)
    1. [@deny_every\(\*args\)](#deny_everyargs)
    1. [@default_rights](#default_rights)
    1. [Naming convention](#naming-convention)
1. [Pauseable](#pauseable)
1. [Fee management](#fee-management)
    1. [Tz entry point](#tz-entry-point)
    1. [Zero Tz contracts](#zero-tz-contracts)
1. [Mirror view](#mirror-view)
1. [Mixins](#mixins)
    1. [Administrator](#administrator)
    1. [Pause](#pause)
    1. [Metadata](#metadata)

<!-- /MarkdownTOC -->


## Import

Everything you need to know about Smartpy is here: [https://smartpy.io/](https://smartpy.io/)
Everything about cli is here: [https://smartpy.io/dev/cli](https://smartpy.io/dev/cli/)


```python
import smartpy as sp
spe = sp.import_script_from_url('....')
```

## Rights management

Rights decorators specify who can call an entry point.

### @admin_only

Verify if sender is administrator

Usage:

1. Init a self.data.administrator (sp.TAddress) attribute with the administrator address. 

```python
class Contract(sp.contract):
    def __init__(self, admin):
	self.init(administrator=admin)
```

2. Add `@spe.admin_only` to each entry point you want to reserve to admin usage.

```python
    @sp.entry_point
    @spe.admin_only
    def manage(self, params):
	# this entry point can only be called by administrator
```

Works well with [Administrator mixin](#administrator)

### @allow_only(\*args)

Allow only calls from of the addresses defined in arguments

Usage: Add `@allow_only(*args)` to the desired entry point with the appropriate allowlist.
   
If arg is a static address: pass it directly

```python
    @sp.entry_point
    @spe.allow_only(sp.test_account("Alice").address)
    def manage(self, params):
	# this entry point can only be called by Alice's address
```

If arg is a static set of address: wrap it into a python set

```python
    @sp.entry_point
    @spe.allow_only({sp.set([sp.test_account("Alice").address, sp.test_account("Bob").address])})
    def manage(self, params):
	# this entry point can only be called by Alice or Bob's address
```

If arg is an address stored in the contract: give the attribute name as a string.

```python
    @sp.entry_point
    @spe.allow_only("manager")
    def manage(self, params):
	# this entry point can only be called by address defined in self.data.manager
```

If arg is a set or the keys of a map stored in the contract: wrap the attribute name as a string in a python set.

```python
    @sp.entry_point
    @spe.allow_only({"managers"})
    def manage(self, params):
	# this entry point can only be called by addresses contained in the self.data.managers attribute
```

You can give multiple arguments.

```python
charlie = sp.test_account("Charlie")

#...

    @sp.entry_point
    @spe.allow_only("administrator"
		    sp.address("tz1..."),
		    charlie,
		    {"allow_set"})
    def manage(self, params):
	# this entry point can only be called by self.data.administrator
	# or the tz1... address
	# or the charlie's address
	# or one of the addresses defined in self.data.allow_set
```

More examples are available here: TODO

### @deny_every(\*args)

Deny if sender is one of the addresses defined in arguments.

Usage: Add `@deny_every` to the desired entry point with the appropriate denylist.

```python
charlie = sp.test_account("Charlie")

#...

    @sp.entry_point
    @spe.deny_every("administrator"
		    sp.address("tz1..."),
		    charlie,
		    {"allow_set"})
    def manage(self, params):
	# this entry point can't be called by self.data.administrator
	# nor by the tz1... address
	# nor by the charlie's address
	# nor by one of the addresses defined in self.data.allow_set
```

The arguments are defined exacly like in [@allow_only](#allow_onlyargs).

### @default_rights

Do nothing. Explicitly declare that you have chosen to not add restriction on purpose.
This permit to use a linter rule like: mandatory use of a right management decorator for each entrypoint.
This way you are sure you've thought about it before releasing. It also help the reviser to detect problems while reading your merge request. 

### Naming convention

We recommend the following naming convention.

When you use `@admin_only` add "admin_" before the name of the entry point.
For example: *admin_setAdministrator*

When you use `@allow_only`, add camel cased name of the allowlist before the name of the entry point.
For example: *registered_manage*.

When you use `@deny_every`, add "not" and camel cased name of the denylist before the entry point.
For example: *notRegistered_register*

In general: `<(not)nameOfAllowed>_<entry_point_name>.`

## Pauseable

A decorators that mark an entrypoint as affected by *self.data.paused*.

Usage: add `@spe.pauseable(admin_bypass)` (arguments is optional)

``` python
    @sp.entry_point
    @spe.pauseable
    def manage(self, params):
        # this entry point is inactive when self.data.paused is True

    @sp.entry_point
    @spe.pauseable(admin_bypass=True)
    def manage2(self, params):
        # this entry point is inactive when self.data.paused is True except for admin
```

## Fee management

### Tz entry point

Indicate that the entry point receive tezzies.
If a value is passed in argument: refuse any other payment.

When used, this decorator replace *@sp.entry_point*.

Usage: add `@spe.tz_entry_point(fee)`
       add `@spe.tz_private_entry_point(fee)` for a private entry point

If fee is a static value: pass is directly

``` python
        @spe.tz_entry_point(sp.tez(5))
        def test(self, params):
            # this entry point accept only payments of 5 tezzies

        @spe.tz_entry_point(sp.mutez(100_000))
        def test2(self, params):
            # this entry point accept only payments of 100,000 mutez
```

If fee is dynamic: give the attribute name as a string

``` python
        @spe.tz_entry_point("registration_fee")
        def test(self, params):
            # this entry point accept only payments equals to self.data.registration_fee
```

### Zero Tz contracts

By default every entry points refuse payment.

The contract fails with "No default" when trying to call the default entry point.
Entry point decorated with `@tz_entry_points"` can still accept tz.

Usage: add `@spe.zero_tz_contract` to the class

``` python
    @spe.zero_tz_contract # every entry point is zero tz by default
    class Contract(sp.Contract):

        @sp.entry_point
        def test(self, params):
            # this entry point refuses tezzies

        @spe.tz_entry_point
        def test2(self, params):
            # this entry point accept tezzies

        @spe.tz_entry_point(sp.tez(5))
        def test3(self, params):
            # this entry point only accept payment of 5 tezzies
```


## Mirror view

View that return the result by callback to the sender and the desired entrypoint. 

When used mirror view **replace** the @sp.view decorator of Smartpy.
You should use it everytime you can.

By sending callbacks to sender it provides a strong security against [Callback authorization bypass](https://forum.tezosagora.org/t/smart-contract-vulnerabilities-due-to-tezos-message-passing-architecture/2045).

Usage: add `@spe.mirror_view(t, message)` (message is optional)

``` python
    @spe.mirror_view(sp.TNat)
    def getBalance(self, params):
        sp.result(self.data.balances[params].balance)
```

This code is a simpler version of the equivalent:

``` python
    @sp.entry_point
    def getBalance(self, params):
        __s3 = sp.local("__s3", self.data.balances[sp.fst(params)].balance)
        sp.transfer(__s3.value, sp.tez(0), sp.contract(sp.TNat, sp.sender, snd(params)).open_some())
```

## Mixins

They are specified as a parent of the contract class.
They provide useful entry points or functions.


### Administrator

Provide a getter and setter entry point for the self.data.administrator address
Provide a is_admin function.
Works well with [`@admin_only`](#admin_only) decorator described above.

Usage: 

``` python
    class Contract(sp.Contract, spe.Administrator):
        def __init__(self, admin):
            self.init(administrator=admin)
```

Provide:

``` python
    @spe.nofee_entry_point
    @spe.admin_only
    def admin_setAdministrator(self, params)

    @spe.nofee_mirror_view(sp.TAddress)
    def getAdministrator(self, params):

    def isAdmin(self, sender):
```

### Pause

Provides a setter entry point for the `self.data.paused` address.
Works well with [Pause](#pause) and [Administrator mixin](#administrator) described above.

Provide:

``` python
    @spe.admin_only
    def admin_setPause(self, params)

    def isPaused(self, sender):

    def set_pauseable(self, pauseable=True, paused_for_admin=True)
```

Usage: 

``` python
    class Contract(sp.Contract, spe.Administrator, spe.Pause):
        def __init__(self, admin):
            self.init(administrator=admin,
                      paused=False)

        @sp.entry_point
        def manage(self, params):
            self.set_pauseable()
            # This entry point is inactivated on pause

        @sp.entry_point
        def manage(self, params):
            self.set_pauseable(paused_for_admin=False)
            # This entry point is inactivated on pause except for admin
```

### Metadata

Provides a setter and remover of [TZIP-16](https://gitlab.com/tzip/tzip/-/blob/master/proposals/tzip-16/tzip-16.md) metadata entry
Works well with [Administrator mixin](#administrator)

Usage: 

``` python
    class Contract(sp.Contract, spe.NoDefault):
        def __init__(self, admin):
            self.init(administrator=admin,
                      metadata=sp.big_map(tkeys=sp.TString, tvalue=sp.TBytes))
```

Provide:

``` python
    @spe.nofee_entry_point
    @spe.admin_only
    def admin_setMetadataEntry(self, params)
        # params.key_ = sp.TString
        # params.value_ = sp.TBytes

    @spe.nofee_entry_point
    @spe.admin_only
    def admin_delMetadataEntry(self, params)
        # params = sp.TString

    @spe.nofee_mirror_view(sp.TBytes)
    def getMetadataEntry(self, params):
        # params = sp.TString
```

